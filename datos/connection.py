__author__ = 'Jona3717'

"""Este módulo contiene los métodos necesarios para las
consultas a la base de datos"""

import sqlite3
import time
import datetime
from .valoraciones import Valoraciones
from sqlite3 import Error


def run_query(query, parameters = ()):
	# recibe la consulta y la envía a la BDD,
	# si la consulta es correcta, devuelve el resultado,
	# de otro modo devuelve el horror
	connection = None
	with sqlite3.connect('Valorar.db') as connection:
		cursor = connection.cursor()
		if parameters:
			result = cursor.execute(query, parameters)
		else:
			result = cursor.execute(query)
		connection.commit()
	return result


def create_tables():
	# Crea las tablas en la BDD si no existen
	query = ("CREATE TABLE IF NOT EXISTS valoraciones(id INTEGER PRIMARY KEY AUTOINCREMENT, " +
			"valoracion TEXT, fecha DATE)")
	run_query(query)
	

def new_valoration(valoration = Valoraciones):
	# Agrega una nueva valoración a la BDD
	data = (valoration.valoracion, valoration.fecha)
	query = ("INSERT INTO valoraciones(valoracion, fecha) VALUES(?,?)")
	run_query(query, data)


def daily_reports(fecha):
	# Crea una consulta que solicita las valoraciones del día y las devuelve en una lista
	valorations = []
	query = ('SELECT * FROM valoraciones WHERE fecha == \'{}\''.format(fecha))
	data = run_query(query).fetchall()
	list = [row for row in data]

	for valoration in list:
		actual = Valoraciones()
		actual.id = valoration[0]
		actual.valoracion = valoration[1]
		actual.fecha = valoration[2]
		valorations.append(actual)

	return valorations


def input_bd(connection):
	# Ingresa una valoración personalizada, se usa a modo de prueba
	if connection:
		cursor = connection.cursor()
		cursor.execute("INSERT INTO valorations VALUES('Buena', 'fecha')")
		connection.commit()
		cursor.close()
		connection.close()

def show():
	# Muestra las valoraciones registradas en la BDD
	query = ('SELECT * FROM valoraciones')
	data = run_query(query).fetchall()
	list = [row for row in data]
	for i in list:
		print(i)


create_tables()