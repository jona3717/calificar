# Calificar

Esta aplicación permite enviar calificaciones para luego realizar un reporte del día y enviarlo por correo. Las calificaciones se realizan a travez de la GUI y el reporte se envía automáticamente a una hora determinada.


### Pre-requisitos 📋

Para el uso adecuado de esta aplicación, se requiere la librería PyQt5.

### Instalación 🔧

No requiere de instalaciones complicadas, solo descargar el código y ejecutar.

```
pyhton valorarUI.py
```

## Ejecutando las pruebas ⚙️

Para probar el envío de reportes, se deben configurar las direcciones de correo del remitente y el receptor en el módulo reportes.

Nota: el correo del remitente debe ser gmail.

```
fromaddr = 'remitente@gmail.com'
toaddrs = 'receptor@email.com'

username = 'remitente@gmail.com'
password = 'clave'
```

La hora de envío de los reórtes puede ser configurada en el método "send_report" en el módulo valorar

## Construido con 🛠️

* [Python](https://www.python.org/) - El lenguaje utilizado para el desarrollo
* [Qt5](https://www.qt.io/) - El framework utilizado para la GUI
* [Sublime Text](https://www.sublimetext.com/) - El editor de código utilizado

## Autores ✒️

* **Jonathan Córdova** - *Desarrollo y documentación* - [jona3717](https://gitlab.com/jona3717)

## Licencia 📄

Este proyecto está bajo la Licencia (GPLv3) - mira el archivo [LICENSE](LICENSE) para detalles

## Expresiones de Gratitud 🎁

* Comenta a otros sobre este proyecto 📢
* Invita una cerveza 🍺 al desarrollador. 
* Da las gracias públicamente 🤓.
* etc.



---
