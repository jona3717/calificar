#!/usr/bin/env python 
__author__ = "Jona3717"

"""Este módulo contiene el método necesario para 
enviar un mensaje por correo"""

import smtplib

fromaddr = 'remitente@gmail.com'
toaddrs  = 'receptor@gmail.com'

username = 'remitente@gmail.com'
password = 'clave'
		 
def send_mail(msg):
	# Recibe un mensaje en forma de string y lo envía de acuerdo s
	# los parámetros establecidos en las variables de la parte superior
	try:
		server = smtplib.SMTP('smtp.gmail.com:587')
		server.starttls()
		server.login(username, password)
		server.sendmail(fromaddr, toaddrs, msg)
		server.quit()
	except Exception:
		print(str(Exception))