#!/usr/bin/env python

__author__ = "Jona3717"

"""Este módulo contiene los la clase con los métodos necesarios para la aplicación
Calificar y para generar y envíar un reporte diario de las valoraciones"""

import os
import sys
import threading
import time
from reportes import send_mail
from datos import connection as con
from datos import valoraciones as Valoraciones


class Valorar:
	# Contiene todos los métodos necesarios para su fincionamiento.
	def __init__(self):
		super().__init__()

		self.send_report()

	def procesos(self):
		# Crea dos hilos e inicia cada uno de ellos
		validar = threading.Thread(target=self.validar)
		send_report = threading.Thread(target=self.send_report)

		send_report.start()
		validar.start()

	def valorar(self):
		# Pide seleccionar una opción mediante ingreso por teclado y lo devuelve
		os.system('clear')

		print('¿Qué le pareció la atención?')
		print('----------------------------\n')

		cont = 0

		for i in self.valoraciones:
		    cont += 1
		    print(cont, self.valoraciones[i])

		opcion = input('\n')
		return opcion

	def validar(self):
		# Toma el valor seleccionado y verifica que sea correcto,
		# de ser así lo registra en la BDD, sino, vuelve a solicitarlo
		valoracion = self.valorar()
		if valoracion.isdigit():
		    valoracion = int(valoracion)
		    os.system('clear')
		    
		    Valoraciones.valoracion = self.valoraciones[valoracion]
		    fecha = time.strftime("%d/%m/%Y")
		    Valoraciones.fecha = fecha
		    con.new_valoration(Valoraciones)
		    print('Muchas gracias,\nsu opinión es importante para nosotros')
		    time.sleep(2)
		    self.validar()
		elif valoracion == 'q':
			sys.exit()
		else:
		    print('opcion inválida')
		    input('Pulse enter para continuar.')
		    self.validar()

	def report(self):
		# Genera un reporte con la fecha y realiza un conteo
		# de cada valoración, luego devuelve un resumen.
		fecha = time.strftime("%d/%m/%Y")
		valorations = con.daily_reports(fecha)
		pesima = 0
		mala = 0
		regular = 0
		buena = 0
		muy_buena = 0
		total = pesima + mala + regular + buena + muy_buena
		for valoration in valorations:
			if valoration.valoracion == 'Pésima':
				pesima += 1
			elif valoration.valoracion == 'Mala':
				mala += 1
			elif valoration.valoracion == 'Regular':
				regular += 1
			elif valoration.valoracion == 'Buena':
				buena += 1
			elif valoration.valoracion == 'Muy buena':
				muy_buena += 1

		message = ('Total de opiniones: {}\n'.format(total) +
			'Pésima: {}\n Mala: {}\n'.format(pesima, mala) +
			'Regular: {}\n Buena: {}\n Muy buena:{}').format(regular, buena, muy_buena)

		return message

	def send_report(self):
		# Verifica la hora y se envía el reporte.
		actual_time = time.strftime("%H:%M")

		if actual_time == '21:30':
			message = self.report()
			send_mail(message)
		else:
			time.sleep(1)
			(self.send_report())


if __name__ == '__main__':
	Valorar()
	sys.exit()