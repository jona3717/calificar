#!/usr/bin/env python

__author__ = 'Jona3717'

"""Este módulo tiene la interfaz gráfica de calificar. Permite evaluar con
cinco distintos valores y envía un reporte de todas las valoraciones hechas
hasta cierta hora del día."""

import sys
import threading
import time
from valorar import Valorar
from datos import connection as con
from datos import valoraciones as Valoraciones
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont, QPixmap, QIcon, QPainter, QColor
from PyQt5.QtWidgets import QApplication, QDialog, QPushButton, QMessageBox

class PushButton(QPushButton):
	# Hereda de QPushButton y crea un boton circular
    def __init__(self, icon, parent=None):
        super(PushButton, self).__init__(parent)

        self.icon = QPixmap(icon)
        self.opacity = QColor(0, 0, 0, 0)

        self.label = ""

        self.setMouseTracking(True)

    def paintEvent(self, event):
    	# Ajusta el tamaño del botón, define la ircunferencia, y ajusta el icono
        width, height = self.width(), self.height()
        icon = self.icon.scaled(width, height, Qt.KeepAspectRatio, Qt.SmoothTransformation)

        painter = QPainter()
        
        painter.begin(self)
        painter.setRenderHint(QPainter.Antialiasing, True)
        painter.setPen(Qt.NoPen)
        painter.drawPixmap(0, 0, icon, 0, 0, 0, 0)
        painter.setPen(Qt.white)
        painter.drawText(event.rect(), Qt.AlignCenter, self.label)
        painter.setPen(Qt.NoPen)
        painter.setBrush(self.opacity)
        painter.drawEllipse(0, 0, width, height)
        painter.end()

        self.setMask(icon.mask())

    def enterEvent(self, event):
    	# Ajusta la opacidad del icono
        self.opacity = QColor(0, 0, 0, 26)

    def leaveEvent(self, event):
    	# Quita la opacidad del icono
        self.opacity = QColor(0, 0, 0, 0)

    

class Window(QDialog):
	# Clase que contiene la GUI de la aplicación
	def __init__(self):
		super().__init__()
		self.valorations = {1 : 'Pésima', 2 :'Mala', 3 : 'Regular', 4 : 'Buena', 5 : 'Muy buena'}

		self.setWindowTitle('Calificar')
		self.setStyleSheet("background-color:lightgray;")
		self.setFixedSize(550, 250)

		self.initUI()

		self.button_pesima.clicked.connect(self.buttonClicked)
		self.button_mala.clicked.connect(self.buttonClicked)
		self.button_regular.clicked.connect(self.buttonClicked)
		self.button_buena.clicked.connect(self.buttonClicked)
		self.button_muy_buena.clicked.connect(self.buttonClicked)

	def initUI(self):
		# Añade los botones de la ventana 
		self.button_pesima = PushButton('img/muy-mal.png', self)
		self.button_mala = PushButton('img/mal.png', self)
		self.button_regular = PushButton('img/regular.png', self)
		self.button_buena = PushButton('img/bien.png', self)
		self.button_muy_buena = PushButton('img/muy-bien.png', self)

		self.button_pesima.setFixedSize(60, 60)
		self.button_mala.setFixedSize(60, 60)
		self.button_regular.setFixedSize(60, 60)
		self.button_buena.setFixedSize(60, 60)
		self.button_muy_buena.setFixedSize(60, 60)

		self.button_pesima.setText('1')
		self.button_mala.setText('2')
		self.button_regular.setText('3')
		self.button_buena.setText('4')
		self.button_muy_buena.setText('5')

		self.button_pesima.move(100, 105)
		self.button_mala.move(175, 105)
		self.button_regular.move(250, 105)
		self.button_buena.move(325, 105)
		self.button_muy_buena.move(400, 105)

	def buttonClicked(self, e):
		# Registra la calificación de acuerdo al botón presionado.
		btn_txt = int(self.sender().text())

		Valoraciones.valoracion = self.valorations[btn_txt]
		fecha = time.strftime("%d/%m/%Y")
		Valoraciones.fecha = fecha
		con.new_valoration(Valoraciones)


if __name__ == "__main__":
	Valorar()

	APP = QApplication(sys.argv)
	FONT = QFont()
	FONT.setPointSize(10)
	FONT.setFamily("Bahnschrift Light")
	APP.setFont(FONT)
	VENTANA = Window()
	VENTANA.show()
	sys.exit(APP.exec_())